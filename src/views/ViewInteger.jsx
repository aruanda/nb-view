'use strict';

var React = require('react');
var numeral = require('numeral');
var FieldMixin = require('nb-helper').nbField;

var ViewInteger = React.createClass({
  mixins: [FieldMixin],

  render: function() {
    var props = this.getViewProperties();

    var notFilled = [null, undefined, ''].indexOf(props.value) > -1;
    var value = notFilled ? <span style={{ color: '#8A8A8A', textAlign: 'center' }}>vazio</span> : numeral(props.value).format('(0)');

    return (
      <div className={props.className}>
        <label>{props.label}</label>
        <div className="form-control-static">{value}</div>
      </div>
    );
  }
});

module.exports = ViewInteger;
