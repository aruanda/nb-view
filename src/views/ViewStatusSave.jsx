'use strict';

var React = require('react');
var FieldMixin = require('nb-helper').nbField;

var ViewPublication = React.createClass({
  mixins: [FieldMixin],

  render: function() {
    var view  = this.props.view;
    var model = this.props.model;
    var nbProps = this.getViewProperties();

    var optionsStatus = view.optionsStatus;
    var withStatus = Boolean(model && model.hasOwnProperty(view.attr) && model[view.attr]);

    var status = withStatus ? model[view.attr] : optionsStatus[0];
    var color  = status === 'ativo' ? 'green' : 'red';

    var statusLabel = nbProps.label.concat('.', status).translate();

    return (
      <div className="nav-tabs-custom">
        <div className="tab-content">
          <p>
            <i className={ 'fa fa-circle fontSize06em text-'.concat(color) }></i>
            &nbsp;&nbsp;
            {statusLabel}
          </p>
        </div>
      </div>
    );
  }
});

module.exports = ViewPublication;
