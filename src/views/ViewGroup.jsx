'use strict';

var React = require('react');

var ViewGroup = React.createClass({
  mountView: function(input, index) {
    var group = this;
    var model = group.props.model;

    var inputValue = model.hasOwnProperty(input.attr) && model[input.attr] ? model[input.attr] : undefined;

    if (!index) return <span key={index}>{inputValue}<br /></span>;
    if (!inputValue) return;

    return (
      <small key={index} style={{ display: 'inline-block', minWidth: '43%', marginRight: '5%' }}>
        <span style={{ marginRight: '10px !important' }} className={input.icon}></span>
        {inputValue}
      </small>
    );
  },

  render: function() {
    var group  = this;
    var views = group.props.view.group.inputs.map(group.mountView);

    return (
      <div className="nav-tabs-custom padding1px15px10px">
        <h2>{views}</h2>
      </div>
    );
  }
});

module.exports = ViewGroup;
