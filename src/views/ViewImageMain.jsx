'use strict';

var React = require('react');

var ViewImageMain = React.createClass({
  render: function() {
    var props = this.props;
    var view  = props.view;
    var model = props.model;
    var file  = model[view.attr];

    if (!file || !file.src) return <span></span>;

    return (
      <div className="nav-tabs-custom">
        <div className="tab-content">
          <img
            src={file.src}
            alt={file.filetitle}
            className="img-responsive pad"
          />
        </div>
      </div>
    );
  }
});

module.exports = ViewImageMain;
