'use strict';

var React = require('react');
var ViewFields = require('./ViewFields');

var ViewContent = React.createClass({
  render: function() {
    var btns  = this.props.btns;
    var model = this.props.model || {};
    var views = this.props.views;

    return (
      <div>
        <section className="content-header overflowHidden">
          <h1 className="pull-left">
            { this.props.domain.concat('.form.title').translate() }
            { ( model && model.id ? ' #'.concat(model.id) : '') }
          </h1>

          <ul className="list-unstyled pull-right las">
            {btns.map(function(btn, index) {
              return (
                <li key={index} className="pull-left marginLeft10px">
                  <button className={btn.className} type={btn.type} onClick={function() {
                    btn.onClick(model);
                  }}>
                    {btn.text.translate()}
                  </button>
                </li>
              );
            })}
          </ul>
        </section>

        <section className="content">
          <ViewFields
            views={views}
            model={model}
          />
        </section>
      </div>
    );
  }
});

module.exports = ViewContent;
