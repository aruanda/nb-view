'use strict';

var React = require('react');
var ViewFields = require('./ViewFields');

var ViewContent = React.createClass({
  render: function () {
    var btns = this.props.btns;
    var model = this.props.model || {};
    var views = this.props.views;

    return React.createElement(
      'div',
      null,
      React.createElement(
        'section',
        { className: 'content-header overflowHidden' },
        React.createElement(
          'h1',
          { className: 'pull-left' },
          this.props.domain.concat('.form.title').translate(),
          model && model.id ? ' #'.concat(model.id) : ''
        ),
        React.createElement(
          'ul',
          { className: 'list-unstyled pull-right las' },
          btns.map(function (btn, index) {
            return React.createElement(
              'li',
              { key: index, className: 'pull-left marginLeft10px' },
              React.createElement(
                'button',
                { className: btn.className, type: btn.type, onClick: function () {
                    btn.onClick(model);
                  } },
                btn.text.translate()
              )
            );
          })
        )
      ),
      React.createElement(
        'section',
        { className: 'content' },
        React.createElement(ViewFields, {
          views: views,
          model: model
        })
      )
    );
  }
});

module.exports = ViewContent;