'use strict';

var React = require('react');

var ViewSuggestSingle = React.createClass({
  render: function () {
    var props = this.props;
    var view = props.view;
    var model = props.model;
    var value = model[view.attr];

    if (!value) return React.createElement('span', null);

    return React.createElement(
      'div',
      { className: 'nav-tabs-custom' },
      React.createElement(
        'div',
        { className: 'tab-content' },
        React.createElement(
          'span',
          { className: 'label label-primary' },
          value.label
        )
      )
    );
  }
});

module.exports = ViewSuggestSingle;