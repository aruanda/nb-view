'use strict';

var React = require('react');
var numeral = require('numeral');
var FieldMixin = require('nb-helper').nbField;

var ViewInteger = React.createClass({
  mixins: [FieldMixin],

  render: function () {
    var props = this.getViewProperties();

    var notFilled = [null, undefined, ''].indexOf(props.value) > -1;
    var value = notFilled ? React.createElement(
      'span',
      { style: { color: '#8A8A8A', textAlign: 'center' } },
      'vazio'
    ) : numeral(props.value).format('(0)');

    return React.createElement(
      'div',
      { className: props.className },
      React.createElement(
        'label',
        null,
        props.label
      ),
      React.createElement(
        'div',
        { className: 'form-control-static' },
        value
      )
    );
  }
});

module.exports = ViewInteger;