'use strict';

var React = require('react');
var FieldMixin = require('nb-helper').nbField;

var ViewSEO = React.createClass({
  mixins: [FieldMixin],

  render: function () {
    var props = this.getViewProperties();
    var seoArgs = props.value || {};

    if (!seoArgs.facebookImage || !seoArgs.facebookImage.hasOwnProperty('src')) seoArgs.facebookImage = { src: 'https://placeholdit.imgix.net/~text?txtsize=33&txt=image&w=600&h=400' };
    if (!seoArgs.twitterImage || !seoArgs.twitterImage.hasOwnProperty('src')) seoArgs.twitterImage = { src: 'https://placeholdit.imgix.net/~text?txtsize=33&txt=image&w=600&h=400' };
    if (!seoArgs.googleImage || !seoArgs.googleImage.hasOwnProperty('src')) seoArgs.googleImage = { src: 'https://placeholdit.imgix.net/~text?txtsize=33&txt=image&w=600&h=400' };

    return React.createElement(
      'div',
      null,
      React.createElement(
        'div',
        { className: 'nav-tabs-custom' },
        React.createElement(
          'div',
          { className: 'tab-content' },
          React.createElement(
            'div',
            { className: 'form-group' },
            React.createElement(
              'label',
              null,
              'Description'
            ),
            React.createElement(
              'p',
              null,
              seoArgs.description
            )
          ),
          React.createElement(
            'div',
            { className: 'form-group' },
            React.createElement(
              'label',
              null,
              'Keywords'
            ),
            React.createElement(
              'p',
              null,
              seoArgs.keywords
            )
          ),
          React.createElement(
            'div',
            { className: 'form-group' },
            React.createElement(
              'label',
              null,
              'Indexação'
            ),
            React.createElement(
              'p',
              null,
              seoArgs.robots
            )
          )
        )
      ),
      React.createElement(
        'div',
        { className: 'nav-tabs-custom' },
        React.createElement(
          'div',
          { className: 'tab-content overflowHidden' },
          React.createElement(
            'div',
            { className: 'box-body col-md-4' },
            React.createElement(
              'div',
              { className: 'col-xs-12 bg-blue-active' },
              React.createElement('i', { className: 'fa fa-facebook-official fontSize18emPadding10px10px10px0' }),
              React.createElement(
                'span',
                { className: 'midiaSocial' },
                'Facebook'
              )
            ),
            React.createElement(
              'div',
              { className: 'col-xs-12 attachment-block clearfix padding15px' },
              React.createElement('img', {
                src: seoArgs.facebookImage.src,
                alt: seoArgs.facebookImage.alt,
                className: 'img-responsive'
              }),
              React.createElement(
                'div',
                { className: 'marginTop20px' },
                React.createElement(
                  'h4',
                  { className: 'attachment-heading text-light-blue' },
                  seoArgs.facebookTitle
                ),
                React.createElement(
                  'div',
                  { className: 'attachment-tex overflowHidden' },
                  React.createElement(
                    'p',
                    null,
                    seoArgs.facebookDescription
                  )
                )
              )
            )
          ),
          React.createElement(
            'div',
            { className: 'box-body col-md-4' },
            React.createElement(
              'div',
              { className: 'col-xs-12 bg-aqua' },
              React.createElement('i', { className: 'fa fa-twitter fontSize18emPadding10px10px10px0' }),
              React.createElement(
                'span',
                { className: 'midiaSocial' },
                'Twitter'
              )
            ),
            React.createElement(
              'div',
              { className: 'col-xs-12 attachment-block clearfix padding15px' },
              React.createElement('img', {
                src: seoArgs.twitterImage.src,
                alt: seoArgs.twitterImage.alt,
                className: 'img-responsive'
              }),
              React.createElement(
                'div',
                { className: 'marginTop20px' },
                React.createElement(
                  'h4',
                  { className: 'attachment-heading text-light-blue' },
                  seoArgs.twitterTitle
                ),
                React.createElement(
                  'div',
                  { className: 'attachment-tex overflowHidden' },
                  React.createElement(
                    'p',
                    null,
                    seoArgs.twitterDescription
                  )
                )
              )
            )
          ),
          React.createElement(
            'div',
            { className: 'box-body col-md-4' },
            React.createElement(
              'div',
              { className: 'col-xs-12 bg-red-active' },
              React.createElement('i', { className: 'fa fa-google-plus fontSize18emPadding10px10px10px0' }),
              ' ',
              React.createElement(
                'span',
                { className: 'midiaSocial' },
                'Google Plus'
              )
            ),
            React.createElement(
              'div',
              { className: 'col-xs-12 attachment-block clearfix padding15px' },
              React.createElement('img', {
                src: seoArgs.googleImage.src,
                alt: seoArgs.googleImage.alt,
                className: 'img-responsive'
              }),
              React.createElement(
                'div',
                { className: 'marginTop20px' },
                React.createElement(
                  'h4',
                  { className: 'attachment-heading text-light-blue' },
                  seoArgs.googleTitle
                ),
                React.createElement(
                  'div',
                  { className: 'attachment-tex overflowHidden' },
                  React.createElement(
                    'p',
                    null,
                    seoArgs.googleDescription
                  )
                )
              )
            )
          )
        )
      )
    );
  }
});

module.exports = ViewSEO;