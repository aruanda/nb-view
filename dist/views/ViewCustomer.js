'use strict';

var React = require('react');
var FieldMixin = require('nb-helper').nbField;

var ViewCustomer = React.createClass({
  mixins: [FieldMixin],

  render: function () {
    return React.createElement(
      'div',
      { className: 'nav-tabs-custom padding1px15px10px' },
      React.createElement(
        'h2',
        null,
        'João Moureira',
        React.createElement('br', null),
        React.createElement(
          'small',
          null,
          React.createElement('span', { className: 'fa fa-whatsapp' }),
          ' 67-9999-9999'
        ),
        '   ',
        React.createElement(
          'small',
          null,
          React.createElement('span', { className: 'fa fa-envelope-o' }),
          ' joão@gmail.com'
        ),
        React.createElement('br', null),
        React.createElement(
          'small',
          null,
          React.createElement('span', { className: 'fa fa-home' }),
          ' Rua do Oeste Vermelho, 189'
        ),
        '   ',
        React.createElement(
          'small',
          null,
          React.createElement('span', { className: 'fa fa-truck' }),
          ' 79013-300'
        ),
        '   ',
        React.createElement(
          'small',
          null,
          React.createElement('span', { className: 'fa fa-map-o' }),
          ' Florianópolis, SC'
        ),
        '   '
      )
    );
  }
});

module.exports = ViewCustomer;