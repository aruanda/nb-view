'use strict';

var React = require('react');
var FieldMixin = require('nb-helper').nbField;

var ViewPublication = React.createClass({
  mixins: [FieldMixin],

  render: function () {
    var view = this.props.view;
    var model = this.props.model;
    var nbProps = this.getViewProperties();

    var optionsStatus = view.optionsStatus;
    var withStatus = Boolean(model && model.hasOwnProperty(view.attr) && model[view.attr]);

    var status = withStatus ? model[view.attr] : optionsStatus[0];
    var color = status === 'ativo' ? 'green' : 'red';

    var statusLabel = nbProps.label.concat('.', status).translate();

    return React.createElement(
      'div',
      { className: 'nav-tabs-custom' },
      React.createElement(
        'div',
        { className: 'tab-content' },
        React.createElement(
          'p',
          null,
          React.createElement('i', { className: 'fa fa-circle fontSize06em text-'.concat(color) }),
          '  ',
          statusLabel
        )
      )
    );
  }
});

module.exports = ViewPublication;