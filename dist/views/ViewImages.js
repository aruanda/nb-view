'use strict';

var React = require('react');
var copy = require('copy-to-clipboard');
var nbLink = require('nb-helper').nbLink;
var FieldMixin = require('nb-helper').nbField;

var ViewImages = React.createClass({
  mixins: [FieldMixin],

  render: function () {
    var props = this.getViewProperties();
    if (!props.value) props.value = [];
    var withoutValue = Boolean(!props.value || !props.value.length);
    if (withoutValue) return React.createElement('div', null);

    return React.createElement(
      'div',
      { className: 'nav-tabs-custom' },
      React.createElement(
        'div',
        { className: 'tab-content overflowHidden' },
        React.createElement(
          'ul',
          { className: 'mailbox-attachments' },
          props.value.map(function (file, index) {
            file.preview = nbLink('files/' + file.filetag + '/' + file.filename);

            var copyImage = function () {
              copy(file.preview);
            };

            return React.createElement(
              'li',
              { key: index },
              React.createElement(
                'span',
                { className: 'mailbox-attachment-icon has-img imgMax132px' },
                React.createElement('img', {
                  src: file.preview,
                  alt: '{file.filetitle}'
                })
              ),
              React.createElement(
                'div',
                { className: 'mailbox-attachment-info' },
                React.createElement(
                  'button',
                  { onClick: copyImage, type: 'button', className: 'btn btn-primary pull-right btn-xs' },
                  React.createElement('i', { className: 'fa fa-copy' }),
                  'copiar url'
                ),
                React.createElement(
                  'a',
                  { href: file.preview, target: '__blank', className: 'mailbox-attachment-name' },
                  React.createElement('i', { className: 'fa fa-camera' }),
                  file.filetitle
                )
              )
            );
          })
        )
      )
    );
  }
});

module.exports = ViewImages;