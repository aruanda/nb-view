'use strict';

var React = require('react');
var moment = require('moment');
var FieldMixin = require('nb-helper').nbField;

var ViewPublication = React.createClass({
  mixins: [FieldMixin],

  render: function () {
    var view = this.props.view;
    var model = this.props.model;
    var nbProps = this.getViewProperties();

    var fieldStatus = view.publication.fieldStatus;
    var fieldDateTime = view.publication.fieldDateTime;
    var optionsStatus = view.publication.optionsStatus;

    var withStatus = Boolean(model && model.hasOwnProperty(fieldStatus) && model[fieldStatus]);
    var withDateTime = Boolean(model && model.hasOwnProperty(fieldDateTime) && model[fieldDateTime]);

    var status = withStatus ? model[fieldStatus] : optionsStatus[0];
    var dateTime = withDateTime ? moment(model[fieldDateTime]).format('dddd, LL') : 'date.not.filled';
    var color = status === 'online' ? 'green' : 'red';

    var statusLabel = nbProps.label.replace(nbProps.attr, fieldStatus).concat('.', status).translate();

    return React.createElement(
      'div',
      { className: 'nav-tabs-custom' },
      React.createElement(
        'div',
        { className: 'tab-content' },
        React.createElement(
          'p',
          null,
          React.createElement('i', { className: 'fa fa-circle fontSize06em text-'.concat(color) }),
          '  ',
          statusLabel
        ),
        React.createElement(
          'p',
          null,
          'Publicação: ',
          dateTime.translate()
        )
      )
    );
  }
});

module.exports = ViewPublication;