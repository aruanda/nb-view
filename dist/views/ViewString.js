'use strict';

var React = require('react');
var FieldMixin = require('nb-helper').nbField;

var ViewString = React.createClass({
  mixins: [FieldMixin],

  render: function () {
    var props = this.getViewProperties();

    var notFilled = [null, undefined, ''].indexOf(props.value) > -1;
    var value = notFilled ? React.createElement(
      'span',
      { style: { color: '#8A8A8A', textAlign: 'center' } },
      'vazio'
    ) : props.value;

    return React.createElement(
      'div',
      { className: props.className },
      React.createElement(
        'label',
        null,
        props.label
      ),
      React.createElement(
        'div',
        { className: 'form-control-static' },
        value
      )
    );
  }
});

module.exports = ViewString;