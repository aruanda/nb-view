'use strict';

var React = require('react');
var numeral = require('numeral');
var FieldMixin = require('nb-helper').nbField;

var ViewTitle = React.createClass({
  mixins: [FieldMixin],

  render: function () {
    var props = this.getViewProperties();
    var valFormated = props.value ? numeral(props.value).format('($0,0.00)') : React.createElement(
      'span',
      { style: { color: '#8A8A8A', textAlign: 'center' } },
      'Valor não informado'
    );

    return React.createElement(
      'div',
      { className: 'nav-tabs-custom' },
      React.createElement(
        'div',
        { className: 'tab-content' },
        valFormated
      )
    );
  }
});

module.exports = ViewTitle;