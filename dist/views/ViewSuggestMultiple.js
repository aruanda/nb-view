'use strict';

var React = require('react');

var ViewSuggestMultiple = React.createClass({
  render: function () {
    var props = this.props;
    var view = props.view;
    var model = props.model;
    var value = model[view.attr];

    if (!value || !value.length) return React.createElement('span', null);

    var colors = ['label-primary', 'label-success', 'label-warning', 'label-info', 'label-danger'];

    return React.createElement(
      'div',
      { className: 'nav-tabs-custom' },
      React.createElement(
        'div',
        { className: 'tab-content' },
        value.map(function (row, index) {
          var idx = index + 1;
          var length = colors.length;

          if (idx > length) idx -= Math.ceil(idx / length) * length;

          var cor = colors[idx - 1];

          return React.createElement(
            'span',
            { key: index, className: 'label '.concat(cor), style: { marginRight: '7px' } },
            row.label
          );
        })
      )
    );
  }
});

module.exports = ViewSuggestMultiple;