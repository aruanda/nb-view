'use strict';

var React = require('react');

var ViewGroup = React.createClass({
  mountView: function (input, index) {
    var group = this;
    var model = group.props.model;

    var inputValue = model.hasOwnProperty(input.attr) && model[input.attr] ? model[input.attr] : undefined;

    if (!index) return React.createElement(
      'span',
      { key: index },
      inputValue,
      React.createElement('br', null)
    );
    if (!inputValue) return;

    return React.createElement(
      'small',
      { key: index, style: { display: 'inline-block', minWidth: '43%', marginRight: '5%' } },
      React.createElement('span', { style: { marginRight: '10px !important' }, className: input.icon }),
      inputValue
    );
  },

  render: function () {
    var group = this;
    var views = group.props.view.group.inputs.map(group.mountView);

    return React.createElement(
      'div',
      { className: 'nav-tabs-custom padding1px15px10px' },
      React.createElement(
        'h2',
        null,
        views
      )
    );
  }
});

module.exports = ViewGroup;