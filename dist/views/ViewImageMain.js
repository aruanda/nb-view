'use strict';

var React = require('react');

var ViewImageMain = React.createClass({
  render: function () {
    var props = this.props;
    var view = props.view;
    var model = props.model;
    var file = model[view.attr];

    if (!file || !file.src) return React.createElement('span', null);

    return React.createElement(
      'div',
      { className: 'nav-tabs-custom' },
      React.createElement(
        'div',
        { className: 'tab-content' },
        React.createElement('img', {
          src: file.src,
          alt: file.filetitle,
          className: 'img-responsive pad'
        })
      )
    );
  }
});

module.exports = ViewImageMain;