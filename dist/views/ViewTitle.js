'use strict';

var React = require('react');
var nbSlug = require('nb-slug');
var nbLink = require('nb-helper').nbLink;
var FieldMixin = require('nb-helper').nbField;

var ViewTitle = React.createClass({
  mixins: [FieldMixin],

  render: function () {
    var props = this.getViewProperties();
    var pathSlug = props.field.pathSlug;
    var model = this.props.model;
    var id = model && model.id ? '-' + String(model.id) : '';
    var slug = pathSlug.concat(nbSlug(props.value) + id);

    var link = React.createElement(
      'a',
      { href: nbLink(slug), target: '_blanck', title: props.value },
      nbLink(slug)
    );

    var notFilled = [null, undefined, ''].indexOf(props.value) > -1;
    var value = notFilled ? React.createElement(
      'span',
      { style: { color: '#8A8A8A' } },
      'Título não informado'
    ) : React.createElement(
      'h1',
      null,
      props.value
    );

    return React.createElement(
      'div',
      { className: 'nav-tabs-custom padding1px15px10px' },
      value,
      link
    );
  }
});

module.exports = ViewTitle;