'use strict';

var React = require('react');
var FieldMixin = require('nb-helper').nbField;

var ViewText = React.createClass({
  mixins: [FieldMixin],

  render: function () {
    var props = this.getViewProperties();

    var notFilled = [null, undefined, ''].indexOf(props.value) > -1 || !String(props.value).trim();

    var value = notFilled ? React.createElement(
      'span',
      { style: { color: '#8A8A8A', textAlign: 'center' } },
      'Texto não informado'
    ) : React.createElement('div', { dangerouslySetInnerHTML: { __html: props.value } });

    return React.createElement(
      'div',
      { className: 'nav-tabs-custom' },
      React.createElement(
        'div',
        { className: 'tab-content' },
        value
      )
    );
  }
});

module.exports = ViewText;